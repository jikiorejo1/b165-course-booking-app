//[SECTION] Packages and Dependencies
  const express = require("express"); 
  const mongoose = require("mongoose");  
  const dotenv = require("dotenv");  
  const courseRoutes = require('./routes/courses'); 
  //acquire the routing components for the users collection
  const userRoutes = require('./routes/users'); 

//[SECTION] Server Setup
  const app = express(); 
  dotenv.config(); 
  app.use(express.json());
  const secret = process.env.CONNECTION_STRING;
  const port = process.env.PORT || 4000; 

//[SECTION] Application Routes
  app.use('/courses', courseRoutes);
  app.use('/users', userRoutes); 

//[SECTION] Database Connection
  mongoose.connect(secret)
  let connectStatus = mongoose.connection; 
  connectStatus.on('open', () => console.log('Database is Connected'));

//[SECTION] Gateway Response
  app.get('/', (req, res) => {
     res.send(`Welcome to FIRST NAME/s CUSTOM NAME APP`); 
  }); 
  app.listen(port, () => console.log(`Server is running on port ${port}`)); 
   
